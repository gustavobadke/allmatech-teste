using Allmatech.Teste.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Allmatech.Teste.FunctionApp
{
    public static class ImoveisFunction
    {
        [FunctionName("Get")]
        public static IActionResult Get(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "imoveis")]
            HttpRequest req,
            ILogger log)
        {
            var client = CosmosDb.Client;
            var uri = CosmosDb.Collections.Imoveis;
            var list = client.CreateDocumentQuery<Imovel>(uri).ToList();

            return new OkObjectResult(list);
        }

        [FunctionName("GetByCode")]
        public static IActionResult GetById(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "imoveis/{id}")]
            HttpRequest req, string id, ILogger log)
        {
            var client = CosmosDb.Client;
            var item = client.CreateDocumentQuery<Imovel>(CosmosDb.Collections.Imoveis).AsEnumerable()
                .Where(p => p.id.ToString() == id).ToList()[0];

            return new OkObjectResult(item);
        }

        [FunctionName("Create")]
        public static async Task<IActionResult> Create(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "imoveis")]
            HttpRequest req, ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var imovel = JsonConvert.DeserializeObject<Imovel>(requestBody);

            var client = CosmosDb.Client;

            var doc = await client.CreateDocumentAsync(CosmosDb.Collections.Imoveis, imovel, disableAutomaticIdGeneration: true);

            return (ActionResult)new OkObjectResult(doc);
        }

        [FunctionName("Update")]
        public static async Task<IActionResult> Update(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "imoveis/{id}")]
            HttpRequest req, [FromRoute] string id, ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var imovel = JsonConvert.DeserializeObject<Imovel>(requestBody);

            var client = CosmosDb.Client;
            var doc = await client.ReplaceDocumentAsync(CosmosDb.GetDocumentUri(CosmosDb.IMOVEIS, id), imovel);

            return new OkObjectResult(doc);
        }

        [FunctionName("Delete")]
        public static async Task<IActionResult> Delete(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "imoveis/{id}")]
            HttpRequest req, [FromRoute] string id, ILogger log)
        {
            var client = CosmosDb.Client;
            var doc = await client.DeleteDocumentAsync(CosmosDb.GetDocumentUri(CosmosDb.IMOVEIS, id));

            return new OkObjectResult(doc);
        }
    }
}