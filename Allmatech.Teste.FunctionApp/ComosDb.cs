﻿using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Allmatech.Teste.FunctionApp
{
    public static class CosmosDb
    {
        const string DATABASE = "Allmatech";
        public const string IMOVEIS = "imoveis";

        static bool isProd = true;

        static string CosmosDbEndPoint
        {
            get
            {
                return isProd 
                    ? "https://gustavo-allmatech.documents.azure.com:443/" 
                    : "https://localhost:8081";
            } 
        }

        static string CosmosDbPrimaryKey
        {
            get
            {
                return isProd 
                    ? "czRl2WZczx0yqNOMy3qPH93C92XTIHmHe4YXLLUyvFNmsm7Hn15VA3nF3QUqx9w4LzVjjMhG8fdsAmqzlFMkeQ==" 
                    : "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";
            }
        }

        public static DocumentClient Client
        {
            get
            {
                return new DocumentClient(new Uri(CosmosDbEndPoint), CosmosDbPrimaryKey);
            }
        }

        public static CosmosDbCollection Collections
        {
            get
            {
                return new CosmosDbCollection();
            }
        }

        public static Uri GetDocumentUri(string collectionName, string documentName) {
            return UriFactory.CreateDocumentUri(DATABASE, collectionName, documentName);
        }

        public class CosmosDbCollection
        {
            Uri GetUriCollection(string collection) {
                return UriFactory.CreateDocumentCollectionUri(DATABASE, collection);
            }

            public Uri Imoveis { get { return GetUriCollection(IMOVEIS); } }
        }
    }
}
