﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Allmatech.Teste.Model
{
    public class Imovel
    {
        public Guid id { get; set; }
        public string Nome { get; set; }
        public string CodigoReferencia { get; set; }

        public string AnuncianteNome { get; set; }
        public string AnuncianteTelefone { get; set; }

        public string EnderecoRua { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public string EnderecoBairro { get; set; }
        public string EnderecoCidade { get; set; }
        public string EnderecoEstado { get; set; }
        public string EnderecoGoogleMapsUrl { get; set; }

        public string Descricao { get; set; }
        public int AreaUtil { get; set; }
        public int QuantidadeBanheiros { get; set; }
        public int QuantidadeQuartos { get; set; }

        public ICollection<string> AreasPrivativas { get; set; }
        public ICollection<string> AreasPublicas { get; set; }

        public Foto FotoDestaque { get; set; }
        public ICollection<Foto> Fotos { get; set; }

    }
}
