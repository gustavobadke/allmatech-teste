﻿using Allmatech.Teste.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Allmatech.Teste.Web.Services
{
    public class ImoveisRest
    {
        string apiUrl;
        HttpClient client;

        public ImoveisRest()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            //apiUrl = "http://localhost:7071/api/imoveis";
            apiUrl = "https://allmatechtestefunctionapp.azurewebsites.net/api/imoveis";
            

            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, new Uri(apiURL));
            //var body = "foo";
            //request.Content = new HttpStringContent(content, Utf8, "application/json");

            //HttpResponseMessage response = await client.SendRequestAsync(request);

            //var responseString = response.Content.ReadAsStringAsync().GetResults();
        }

        public async Task<IEnumerable<Imovel>> Get() {
            var msg = await client.GetAsync(apiUrl);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<Imovel>>(
                await msg.Content.ReadAsStringAsync());
        }

        public async Task<Imovel> Get(Guid id)
        {
            var msg = await client.GetAsync(apiUrl + $"/{id}");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Imovel>(
                await msg.Content.ReadAsStringAsync());
        }

        public async void Post(Imovel imovel)
        {
            await client.PostAsJsonAsync(apiUrl, imovel);
        }

        public async void Put(Imovel imovel)
        {
            await client.PutAsJsonAsync(apiUrl + $"/{imovel.id}", imovel);
        }

        public async void Delete(Guid id)
        {
            await client.DeleteAsync(apiUrl + $"/{id}");
        }
    }
}
