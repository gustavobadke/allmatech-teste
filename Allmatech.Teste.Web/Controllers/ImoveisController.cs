﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Allmatech.Teste.Model;
using Allmatech.Teste.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Allmatech.Teste.Web.Controllers
{
    public class ImoveisController : Controller
    {
        // GET: Imoveis
        public async Task<ActionResult> Index()
        {
            try
            {
                var rest = new ImoveisRest();
                var imoveis = await rest.Get();
                return View(imoveis);
            }
            catch (Exception)
            {
                return View(new List<Imovel>());
            }
        }

        // GET: Imoveis/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            try
            {
                var rest = new ImoveisRest();
                var imovel = await rest.Get(id);
                return View(imovel);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Imoveis/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Imoveis/Create
        [HttpPost]
        public ActionResult Create(Imovel imovel)
        {
            try
            {
                var rest = new ImoveisRest();
                imovel.id = Guid.NewGuid();
                rest.Post(imovel);
                
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Imoveis/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            try
            {
                var rest = new ImoveisRest();
                var imovel = await rest.Get(id);
                return View(imovel);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Imoveis/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, Imovel imovel)
        {
            try
            {
                var rest = new ImoveisRest();
                rest.Put(imovel);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Imoveis/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                var rest = new ImoveisRest();
                rest.Delete(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}